<p align="center">
<img src="/LogoOfficial.png" width="300" >
</p>

## Incca interview

### Intro

Hi! If you wanna work with us, you gotta solve this first. Let's see what you've got!

### Instructions

1. This test should be forked, worked on, and then commited to Github, with every comment in **english**.
2. Every applicant should solve **all** the general questions.
3. A candidate must resolve the activities that have been proposed.
4. Solving everything is a bonus :)
5. When you finish this test, send an e-mail containing the repository link and the salary expectations to <qualidade@incca.com.br>
6. This test's instructions should be enough.
7. you have until the date 25/05
8. Happy Coding!

### General

### Frontend

Using your favorite programming language and framework, lib and others:

1. **Create an application.**
-  1.1 create a name for application.
-  1.2 create logo and design and color combination.
-  1.3 the application can have 3 pages.
-  1.4 use git-hub API.

2. **Application details**
Screen should receive the user through an input and a button to access the repositories of this user, and when clicking on the repository, issues of the specific repository should appear.
- 2.1. screen should receive a user and add it to the main screen and saved on localstorage but he must enter the next repositories screen.
  - 2.1.1. you can remove users recorded on the local storage
- 2.2. Repository screen should list the repository and when clicking on the repository you should go to one with details of this repository.
<br>Obs. **Leave the instructions on how to execute your project and be ready to execute it on the return of the interview ok, good luck**


### Backend

_I know, I know... It looks easy compared to the frontend challenge; but don't be fooled! We expect **more** from your answers in here! We will evaluate **how** you answer, and your answer's organization and structure!_

Answer the following questions:
- Suppose you're working with 3 people on a project. What would be **the** ideal software development process?

- How do you think an entity relationship diagram for _Instagram_ would be like?

- Now that you have imagined the ER diagram, **how** would you build _Instagram_ from scratch?

That's it! Thanks for doing this test!

🚀
